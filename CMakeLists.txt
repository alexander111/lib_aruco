cmake_minimum_required(VERSION 2.8.3)

set(PROJECT_NAME lib_aruco)
project(${PROJECT_NAME})

### Use version 2011 of C++ (c++11). By default ROS uses c++98
#see: http://stackoverflow.com/questions/10851247/how-to-activate-c-11-in-cmake
#see: http://stackoverflow.com/questions/10984442/how-to-detect-c11-support-of-a-compiler-with-cmake
#add_definitions(-std=c++11)
#add_definitions(-std=c++0x)
add_definitions(-std=c++03)

# Set the build type.  Options are:
#  Coverage       : w/ debug symbols, w/o optimization, w/ code-coverage
#  Debug          : w/ debug symbols, w/o optimization
#  Release        : w/o debug symbols, w/ optimization
#  RelWithDebInfo : w/ debug symbols, w/ optimization
#  MinSizeRel     : w/o debug symbols, w/ optimization, stripped binaries




#STANDARD ARUCO LIB
set(LIB_ARUCO_SOURCE_DIR
	src/source) 
	
set(LIB_ARUCO_INCLUDE_DIR
	src/include)

set(LIB_ARUCO_SOURCE_FILES
        ${LIB_ARUCO_SOURCE_DIR}/arucofidmarkers.cpp
        ${LIB_ARUCO_SOURCE_DIR}/cvdrawingutils.cpp
        ${LIB_ARUCO_SOURCE_DIR}/marker.cpp
        ${LIB_ARUCO_SOURCE_DIR}/markerdetector.cpp
        ${LIB_ARUCO_SOURCE_DIR}/cameraparameters.cpp
        ${LIB_ARUCO_SOURCE_DIR}/boarddetector.cpp
        ${LIB_ARUCO_SOURCE_DIR}/board.cpp
)
 
set(LIB_ARUCO_HEADER_FILES
        ${LIB_ARUCO_INCLUDE_DIR}/aruco.h
        ${LIB_ARUCO_INCLUDE_DIR}/arucofidmarkers.h
        ${LIB_ARUCO_INCLUDE_DIR}/cvdrawingutils.h
        ${LIB_ARUCO_INCLUDE_DIR}/marker.h
        ${LIB_ARUCO_INCLUDE_DIR}/markerdetector.h
        ${LIB_ARUCO_INCLUDE_DIR}/boarddetector.h
        ${LIB_ARUCO_INCLUDE_DIR}/cameraparameters.h
        ${LIB_ARUCO_INCLUDE_DIR}/board.h
        ${LIB_ARUCO_INCLUDE_DIR}/exports.h
)


find_package(catkin REQUIRED)

#opencv
find_package(OpenCV REQUIRED)


catkin_package(
	INCLUDE_DIRS ${LIB_ARUCO_INCLUDE_DIR}
        LIBRARIES lib_aruco
	DEPENDS OpenCV
  )


include_directories(${LIB_ARUCO_INCLUDE_DIR})
include_directories(${OpenCV_INCLUDE_DIRS})
include_directories(${catkin_INCLUDE_DIRS})



add_library(lib_aruco ${LIB_ARUCO_SOURCE_FILES} ${LIB_ARUCO_HEADER_FILES})
add_dependencies(lib_aruco ${catkin_EXPORTED_TARGETS})
target_link_libraries(lib_aruco ${OpenCV_LIBS})
target_link_libraries(lib_aruco ${catkin_LIBRARIES})

